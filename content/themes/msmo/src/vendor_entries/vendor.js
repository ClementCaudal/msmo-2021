// All imports in this file will be compiled into vendors.js file.
//
// Note: ES6 support for these imports is not supported in base build

module.exports = [
	'./node_modules/jquery/dist/jquery.js',
	'./node_modules/bootstrap/dist/js/bootstrap.bundle.js',
	'./node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js',
	 './node_modules/imagesloaded/imagesloaded.pkgd.js',
	 './node_modules/masonry-layout/dist/masonry.pkgd.js',
	 './node_modules/owl.carousel/dist/owl.carousel.min.js',
	 './node_modules/scrollreveal/dist/scrollreveal.js',
	 './node_modules/ion-rangeslider/js/ion.rangeSlider.min.js',
	 './node_modules/bootstrap-select/dist/js/bootstrap-select.min.js',
	 './node_modules/leaflet/dist/leaflet.js',
];
